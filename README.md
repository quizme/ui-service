# ui-service

The ui-service is responsible for displaying restaurant details using Angular 12.

# Prerequisites
Latest version of NodeJS and npm

# To build the project
npm install

# To run the service
npm start

# To start the entire setup in "docker" ENV
Please refer **deployment-scripts** folder

# To view in browser
http://localhost:4200
