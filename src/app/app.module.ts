import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {TopBarComponent} from './top-bar/top-bar.component';
import {RestaurantListComponent} from './restaurant-list/restaurant-list.component';
import {RestaurantDataService} from './restaurant-list/service/restaurant-data.service';


@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      {path: '', component: RestaurantListComponent},
    ])
  ],
  declarations: [
    AppComponent,
    TopBarComponent,
    RestaurantListComponent
  ],
  bootstrap: [
    AppComponent
  ],
  providers: [RestaurantDataService]
})
export class AppModule {
}
