interface Page {
  size: number;
  totalElements: number;
  totalPages?: number;
  number: number;
}

interface Link {
  rel: string;
  href: string;
}

interface Restaurant {
  name: string;
  address: string;
  city: string;
  formattedMenu?: string[];
  cuisines?: string[];
  recommendations?: string[];
}

export interface RestaurantPageResponse {
  content: Restaurant[];
  page: Page;
  links: Link[];
}
