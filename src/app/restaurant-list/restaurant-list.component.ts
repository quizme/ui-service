import {Component, OnDestroy, OnInit} from '@angular/core';
import {RestaurantDataService} from './service/restaurant-data.service';
import {RestaurantPageResponse} from './model/restaurant';
import {HttpErrorResponse} from '@angular/common/http';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.css']
})
export class RestaurantListComponent implements OnInit, OnDestroy {

  restaurants: RestaurantPageResponse | undefined;
  apiFetchTime: any;
  city = '';
  cuisine = '';

  private restaurantDataSubscription: Subscription | undefined;

  constructor(private restaurantDataService: RestaurantDataService) {
  }

  ngOnInit() {
  }

  onCityEntered(evenTarget: any) {
    this.city = evenTarget.value;
    this.search();
  }

  onCuisineSSearch(eventTarget: any) {
    this.cuisine = eventTarget.value;
    this.search();
  }

  private search() {
    if (this.city && this.cuisine) {
      this.restaurantDataSubscription = this.restaurantDataService
        .getRestaurants(this.city, this.cuisine)
        .subscribe(
          (data: RestaurantPageResponse) => {
            this.handlerRestaurantsResponse(data);
          },
          (error: HttpErrorResponse) => window.alert('Error occurred! Try again later!')
        );
    } else {
      window.alert('Please enter city and cuisine');
    }
  }


  private handlerRestaurantsResponse(data: RestaurantPageResponse) {
    this.restaurants = data;
    if (data) {
      if (data.links && data.links.length > 0) {
        const link = data.links.find(l => 'executionTimeInMs' === l.rel);
        if (link) {
          this.apiFetchTime = link.href;
        }
      }
    } else {
      window.alert('No data found!');
    }
  }

  prev() {
    this.getPrevOrNextPage('prev');
  }

  next() {
    this.getPrevOrNextPage();
  }

  private getPrevOrNextPage(prevOrNext = 'next') {
    if (this.restaurants && this.restaurants.links) {
      const link = this.restaurants.links.find(l => prevOrNext === l.rel);
      if (link) {
        this.restaurantDataSubscription = this.restaurantDataService
          .getPrevOrNextRestaurants(link.href)
          .subscribe(
            (data: RestaurantPageResponse) => {
              this.handlerRestaurantsResponse(data);
            },
            (error: HttpErrorResponse) => window.alert('Error occurred! Try again later!')
          );
      } else {
        window.alert(`No ${prevOrNext} page available!`);
      }
    }
  }

  ngOnDestroy() {
    if (this.restaurantDataSubscription) {
      this.restaurantDataSubscription.unsubscribe();
    }
  }
}
