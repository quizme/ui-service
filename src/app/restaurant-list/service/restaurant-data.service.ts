import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {RestaurantPageResponse} from '../model/restaurant';
import {Observable} from 'rxjs';

@Injectable()
export class RestaurantDataService {
  private restaurantUrl = '/api/restaurants';
  constructor(private http: HttpClient) { }

  getRestaurants(city: string, cuisine: string): Observable<RestaurantPageResponse> {
    return this.http.get<RestaurantPageResponse>(`${this.restaurantUrl}?city=${city}&cuisine=${cuisine}`);
  }

  getPrevOrNextRestaurants(url: string): Observable<RestaurantPageResponse> {
    return this.http.get<RestaurantPageResponse>(url);
  }
}
